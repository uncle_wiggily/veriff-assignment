## Veriff FE Test Assignment
This is a front-end Test Assignment for [Veriff](https://www.veriff.com/).

#### List of main functionality
- auto-generated form with yes/no questions with custom validation
- form supports navigation with arrow keys (up & down) and shortcut for answers yes-1 / no-2
- on form submission, generate payload and "send to BE"

### Used technologies
- React
- Typescript
- Styled-components (why? they are just great for - [example article](https://medium.com/building-crowdriff/styled-components-to-use-or-not-to-use-a6bb4a7ffc21))
- Testing - [react-testing-library](https://testing-library.com/) and it's companions (jest-dom, user-event)
    - _tests cover essential functionality (basic rendering and simple flows are not covered for the sake of simplicity)_

### Running
Run locally ```npm run start```

Run tests ```npm run test```
