import React, { useState } from "react";
import styled from "styled-components";

import ValidationForm from "./components/ValidationForm";
import ErrorPage from "./components/ErrorPage";
import SuccessPage from "./components/SuccessPage";

export default function App() {
    const [view, setView] = useState('form');

    const getView = () => {
        let View;

        switch (view) {
            case 'form':
                View = ValidationForm;
                break;
            case 'error':
                View = ErrorPage;
                break;
            case 'success':
                View = SuccessPage;
                break;
            default:
                View = ValidationForm;
        }

        return <View changeView={setView} />;
    }

    return (
        <AppContainer>
            <FormContainer>
                {getView()}
            </FormContainer>
        </AppContainer>
    );
}

const AppContainer = styled.div`
    font-family: sans-serif;
    
    --primary-color: #004e5f;
    --highlight-color: #def7f7;
`

const FormContainer = styled.div`
  width: 376px;
  margin: 160px auto;
  padding: 8px;

  @media (max-width:680px) {
    width: 320px;
    margin: 80px auto;
    padding: 16px;
  }
`
