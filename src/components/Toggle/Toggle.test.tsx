import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import Toggle from "./index";

const defaultProps = {
    toggleValue: "",
    onChange: () => {},
}

describe("Toggle", () => {

    it("should render 2 options", () => {
        const { getByText } = render(<Toggle {...defaultProps} />);

        expect(getByText("yes")).toBeDefined();
        expect(getByText("no")).toBeDefined();
    });

    it("should correctly select Yes option", () => {
        const props = {
            ...defaultProps,
            onChange: jest.fn(),
        }
        const { getByText } = render(<Toggle {...props} />);

        fireEvent.click(getByText("yes"));

        expect(props.onChange).toBeCalledTimes(1);
        expect(props.onChange).toBeCalledWith("yes");
    });

    it("should correctly select No option", () => {
        const props = {
            ...defaultProps,
            onChange: jest.fn(),
        }
        const { getByText } = render(<Toggle {...props} />);

        fireEvent.click(getByText("no"));

        expect(props.onChange).toBeCalledTimes(1);
        expect(props.onChange).toBeCalledWith("no");
    });

});
