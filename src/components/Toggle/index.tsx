import React from "react";
import styled from "styled-components";

interface Props {
    toggleValue: string;
    onChange: (result: string) => void;
}

const Toggle: React.FC<Props> = ({ toggleValue, onChange }) => {
    return (
        <div>
            <ToggleButton left selected={toggleValue === 'yes'} onClick={() => onChange("yes")}>yes</ToggleButton>
            <ToggleButton selected={toggleValue === 'no'} onClick={() => onChange("no")}>no</ToggleButton>
        </div>
    );
}

const ToggleButton = styled.button<any>`
  font-weight: 500;
  font-size: 14px;
  line-height: 30px;
  letter-spacing: 0.4px;
  text-transform: capitalize;
  text-align: center;
  user-select: none;
  border: 1px solid var(--primary-color);
  border-left: ${p => !p.left && 0};
  background-color: ${p => p.selected ? 'var(--primary-color)' : 'transparent'};
  color: ${p => p.selected ? 'white' : 'var(--primary-color)'};

  position: relative;
  height: 32px;
  width: 64px;
  padding: 0 16px;
  border-radius: ${p => p.left ? '4px 0 0 4px' : '0 4px 4px 0'};
  cursor: pointer;
`

export default Toggle;
