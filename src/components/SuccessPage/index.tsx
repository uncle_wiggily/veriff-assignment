import React from "react";
import styled from "styled-components";

const SuccessPage = () => {
    return (
        <Wrapper>
            <p>Thank you for using</p>
            <img
                src="https://thewealthmosaic.s3.amazonaws.com/media/Logo_Veriff.png"
                alt="veriff logo"
                height="48"
            />
        </Wrapper>
    )
};

const Wrapper = styled.div`
  text-align: center;
`

export default SuccessPage;
