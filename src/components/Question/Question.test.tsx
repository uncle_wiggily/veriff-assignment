import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event'

import Question from "./index";

const defaultProps = {
    check: {
        id: "aaa",
        priority: 10,
        description: "Very important question?",
        result: "yes",
    },
    isDisabled: false,
    focused: true,
    onChange: () => {},
}

describe("Question", () => {

    it("should have description and options toggle", () => {
        const { getByText } = render(<Question {...defaultProps} />);

        expect(getByText(defaultProps.check.description)).toBeDefined();
        expect(getByText("yes")).toBeDefined();
        expect(getByText("no")).toBeDefined();
    });

    it("should not receive user events if disabled", () => {
        const props = {
            ...defaultProps,
            isDisabled: true,
            onChange: jest.fn(),
        }
        const { getByText } = render(<Question {...props} />);

        try {
            userEvent.click(getByText("yes"))
        } catch (e) {
            expect(e.message).toBe("unable to click element as it has or inherits pointer-events set to \"none\".")
        }
    });

    it("should change color on hover", () => {
        const { getByText } = render(<Question {...defaultProps} />);

        const el = getByText(defaultProps.check.description).parentElement;

        expect(el).toBeDefined();

        fireEvent.mouseOver(el as any);
        expect(el).toHaveStyle(`background-color: #E3F6F7`);
    });

});
