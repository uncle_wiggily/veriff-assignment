import React from "react";
import styled from "styled-components";

import Toggle from "../Toggle";
import { Check } from "../../types";

interface Props {
    check: Check;
    isDisabled: boolean;
    focused: boolean;
    onChange: (checkId: string, result: string) => void;
}

const Question: React.FC<Props> = ({ check, isDisabled, focused, onChange }) => {
    const toggleQuestion = (result: string) => {
        onChange(check.id, result)
    }

    return (
        <QuestionContainer disabled={isDisabled} isFocused={focused}>
            <QuestionTitle>{check.description}</QuestionTitle>
            <Toggle onChange={toggleQuestion} toggleValue={check.result || ''} />
        </QuestionContainer>
    )
}

const QuestionContainer = styled.div<any>`
    padding: 8px;
    
    ${p => p.disabled && `
        opacity: 0.25;
        pointer-events: none;
    `}
  
    ${p => p.isFocused && 'background-color: #E3F6F7'};
  
    :hover {
        background-color: #E3F6F7
    }
`

const QuestionTitle = styled.p`
  margin-top: 0;
  margin-bottom: 8px;
`

export default Question;
