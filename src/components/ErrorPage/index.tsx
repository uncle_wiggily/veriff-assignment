import React from "react";
import styled from "styled-components";

interface Props {
    changeView: (view: string) => void,
}

const ErrorPage: React.FC<Props> = ({ changeView }) => {
    return (
        <Wrapper>
            <p>Oopsie, something failed on our side, please try again.</p>
            <StyledButton onClick={() => changeView('form')}>
                Refresh
            </StyledButton>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    text-align: center;
`

const StyledButton = styled.button`
    height: 32px;
    width: 96px;
    margin: 0 auto;
  
    background-color: var(--primary-color);
    color: white;
    border: 1px solid transparent;
    border-radius: 3px;
    cursor: pointer;

  :enabled:hover,
    :enabled:focus {
        filter: brightness(110%);
    }
`

export default ErrorPage;
