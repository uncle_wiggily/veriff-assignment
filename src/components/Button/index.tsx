import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const propTypes = {
    children: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    onClick: PropTypes.func,
    loading: PropTypes.bool,
}

type Props = PropTypes.InferProps<typeof propTypes>;

const Button: React.FC<Props> = ({
                                     children,
                                     loading,
                                     ...rest
                                 }) => {
    return (
        <StyledButton isLoading={loading} {...rest}>
            {children}
        </StyledButton>
    );
};

Button.propTypes = propTypes;

const StyledButton: any = styled.button<any>`
    font-weight: 500;
    font-size: 14px;
    line-height: 40px;
    letter-spacing: 0.4px;
    text-transform: uppercase;
    text-align: center;
    user-select: none;
    border: 1px solid transparent;
    background-color: var(--primary-color);
    color: white;
  
    position: relative;
    height: 40px;
    padding: 0 16px;
    border-radius: 4px;
    cursor: pointer;
  
    :disabled {
      background-color: #e6ebf4;
      color: #bdc0cf;
      cursor: not-allowed;
    }
  
    :enabled:hover,
    :enabled:focus {
      filter: brightness(110%);
    }
  
    ${p => p.isLoading && 'animation: backgroundTransition 2.5s ease-in-out infinite;'}

    @keyframes backgroundTransition {
        0% { background-color: var(--primary-color); }
        50% { background-color: #4dc8e3; }
        100% { background-color: var(--primary-color); }
    }
`

export default Button;
