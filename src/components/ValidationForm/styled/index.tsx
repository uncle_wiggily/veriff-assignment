import styled from "styled-components";

export const FormWrapper = styled.div`
    display: flex;
  
    flex-direction: column;
`

export const SubmitBtnWrapper = styled.div`
    margin-top: 16px;
    margin-left: auto;
    margin-right: 16px;
`

export const ErrorText = styled.div`
    margin: 16px auto;
  
    font-size: 14px;
    color: #d14324;
`