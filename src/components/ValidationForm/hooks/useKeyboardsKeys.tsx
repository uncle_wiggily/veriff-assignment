import { useCallback, useEffect, useState } from "react";

import { canMoveDown, canMoveUp } from "../utils";
import { Check } from "../../../types";

const useKeyboardsKeys = (
    checks: Check[],
    handleCheckAnswer: (checkId: string, result: string) => void,
) => {
    const [questionFocus, setQuestionFocus] = useState(-1);

    const handleKeyDown = useCallback(event => {
        const { key } = event;
        event.preventDefault();

        if (questionFocus < 0) {
            if (key === 'ArrowDown') {
                setQuestionFocus(0);
            }
            return;
        }

        if (canMoveDown(key, checks, questionFocus)) {
            setQuestionFocus((f: number) => f + 1);
        } else if (canMoveUp(key, checks, questionFocus)) {
            setQuestionFocus((f: number) => f - 1);
        } else if (key === '1' || key === '2') {
            const focusedCheck = checks[questionFocus];
            const result = key === '1' ? 'yes' : 'no';
            handleCheckAnswer(focusedCheck.id, result);
        }
    }, [checks, questionFocus, handleCheckAnswer]);

    useEffect(() => {
        window.addEventListener("keydown", handleKeyDown);
        return () => {
            window.removeEventListener("keydown", handleKeyDown);
        };
    }, [handleKeyDown]);

    return questionFocus;
}

export default useKeyboardsKeys;
