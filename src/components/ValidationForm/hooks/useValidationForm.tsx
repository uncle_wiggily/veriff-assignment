import { useState, useEffect } from 'react';

import { Check } from "../../../types";
import { countPositiveAnswers } from "../utils";

const useValidationForm = () => {
    const [checks, setChecks] = useState<Check[]>([]);
    const [submittable, setSubmittable] = useState(false);

    useEffect(() => {
        const negativeAnswer = checks.find((check: Check) => check.result === 'no')

        if (negativeAnswer) {
            setSubmittable(true);
            return;
        }

        if (countPositiveAnswers(checks) === checks.length) {
            setSubmittable(true);
        } else {
            setSubmittable(false);
        }
    }, [checks])

    const handleCheckAnswer = (checkId: string, result: string) => {
        const selectedCheck = checks.find((check: Check) => check.id === checkId);

        if (selectedCheck) {
            selectedCheck.result = result;
            setChecks([...checks]);
        }
    };

    return {
        checks,
        setChecks,
        handleCheckAnswer,
        submittable,
    }
};

export default useValidationForm;
