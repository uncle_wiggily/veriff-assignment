import React, { useEffect, useState } from "react";

import { fetchChecks, submitCheckResults } from '../../api/api';
import Question from "../Question";
import Button from "../Button";
import useValidationForm from "./hooks/useValidationForm";
import { formatSubmitPayload, shouldShowQuestion, sortChecks } from "./utils";
import { Check } from "../../types";

import { ErrorText, FormWrapper, SubmitBtnWrapper } from "./styled";
import useKeyboardsKeys from "./hooks/useKeyboardsKeys";

interface Props {
    changeView: (view: string) => void;
}

const ValidationForm: React.FC<Props> = ({ changeView }) => {
    const { checks, setChecks, handleCheckAnswer, submittable } = useValidationForm();
    const questionFocus = useKeyboardsKeys(checks, handleCheckAnswer);
    const [submitError, setSubmitError] = useState<any>(false);
    const [loading, setLoading] = useState<any>(false);

    useEffect(() => {
        const fetch = async () => {
            try {
                const checks: Check[] = await fetchChecks();
                const sortedChecks: Check[] = sortChecks(checks);
                setChecks(sortedChecks);
            } catch (e) {
                changeView('error');
            }
        }
        fetch()
        // eslint-disable-next-line
    }, [])

    const submitChecks = async () => {
        const results: any = formatSubmitPayload(checks);
        setLoading(true);
        try {
            await submitCheckResults(results);
            setLoading(false);
            changeView('success');
        } catch {
            setLoading(false);
            setSubmitError(true);
        }
    }

    return (
        checks.length > 0 ?
            <FormWrapper data-testid="validation-form">
                {
                    sortChecks(checks).map( (check: Check, idx: number) => (
                        <Question
                            key={check.id}
                            check={check}
                            focused={idx === questionFocus}
                            isDisabled={!shouldShowQuestion(checks, idx)}
                            onChange={handleCheckAnswer}
                        />
                    ))
                }
                <SubmitBtnWrapper>
                    <Button
                        disabled={!submittable}
                        loading={loading}
                        onClick={() => submitChecks()}
                    >
                        Submit
                    </Button>
                </SubmitBtnWrapper>
                {submitError &&
                    <ErrorText>
                        Something went wrong. Please try submitting again.
                    </ErrorText>
                }
            </FormWrapper>
            : null
    )
}

export default ValidationForm;
