import { Check } from "../../types";

export const sortChecks = (checks: any[]) => checks.sort((a: any, b: any) => b.priority - a.priority)

export const shouldShowQuestion = (checks: Check[], idx: number): boolean => {
    if (idx === 0) return true;

    let shouldShow = true;
    for (let i = 0; i < idx; i++) {
        if (checks[i].result !== 'yes') {
            shouldShow = false;
            break;
        }
    }
    return shouldShow;
}

export const canMoveDown = (key: string, checks: Check[], idx: number) => {
    return key === 'ArrowDown'
        && idx < checks.length - 1
        && shouldShowQuestion(checks, idx + 1);
}

export const canMoveUp = (key: string, checks: Check[], idx: number) => {
    return key === 'ArrowUp' && idx > 0
}

export const countPositiveAnswers = (checks: Check[]) => {
    const positiveAnswers = checks.filter((check: Check) => check.result === 'yes');
    return positiveAnswers.length;
}

export const formatSubmitPayload = (checks: Check[]) => {
    return checks.map((check: Check) => ({
        checkId: check.id,
        result: check.result || null,
    }));
}
