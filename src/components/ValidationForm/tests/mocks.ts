export const mockChecks = [
    {
        id: "aaa",
        priority: 10,
        description: "Face on the picture matches face on the document"
    },
    {
        id: "ccc",
        priority: 7,
        description: "Face is clearly visible"
    },
    {
        id: "bbb",
        priority: 5,
        description: "Veriff supports presented document"
    },
];

export const mockCorrectRequestPayload = [
    {
        checkId: "aaa",
        result: "no"
    },
    {
        checkId: "ccc",
        result: null,
    },
    {
        checkId: "bbb",
        result: null,
    }
];
