import React from 'react';
import { render, waitFor, within } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import * as api from "../../../api/api";

import { mockChecks, mockCorrectRequestPayload } from "./mocks";
import ValidationForm from "../index";

const defaultProps = {
    changeView: () => {},
};

const validationFormId = "validation-form";

jest.mock('../../../api/api', () => ({
    fetchChecks: jest.fn(),
    submitCheckResults: jest.fn(),
}));

describe("ValidationForm", () => {

    beforeEach(() => {
        const checksArray = mockChecks.map(c => ({ ...c }));
        api.fetchChecks.mockResolvedValue(checksArray);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it("renders correct amount of checks", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        for (let i = 0; i < mockChecks.length; i++) {
            const currCheck = mockChecks[i];

            expect(getByText(currCheck.description)).toBeDefined();
        }
    });

    it("submit button should be disabled by default", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        expect(getByText("Submit")).toBeDisabled();
    });

    it("submit button should be available after No answer", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        const firstQuestion = getByText(mockChecks[0].description).parentElement;
        const noOption = within(firstQuestion).getByText("no");

        userEvent.click(noOption);

        expect(getByText("Submit")).not.toBeDisabled();
    });

    it("submit button should be only available if all question have Yes answer", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        const firstQuestion = getByText(mockChecks[0].description).parentElement;
        const yesOption = within(firstQuestion).getByText("yes");

        userEvent.click(yesOption);
        expect(getByText("Submit")).toBeDisabled();

        for (let i = 1; i < 3; i++) {
            const question = getByText(mockChecks[i].description).parentElement;
            const yesOption = within(question).getByText("yes");
            userEvent.click(yesOption);
        }

        expect(getByText("Submit")).toBeEnabled();
    });

    it("on submit should send correct payload", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        const firstQuestion = getByText(mockChecks[0].description).parentElement;
        const noOption = within(firstQuestion).getByText("no");

        userEvent.click(noOption);
        userEvent.click(getByText("Submit"));

        expect(api.submitCheckResults).toBeCalledTimes(1);
        expect(api.submitCheckResults).toBeCalledWith(mockCorrectRequestPayload)

        await waitFor(() => getByTestId(validationFormId));
    });
});

describe("ValidationForm (Keyboard actions)", () => {
    beforeEach(() => {
        const checksArray = mockChecks.map(c => ({ ...c }));
        api.fetchChecks.mockResolvedValue(checksArray);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it("should be able to start selection with keyboard and select No", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        userEvent.keyboard("{ArrowDown}");
        userEvent.keyboard("Digit$2");

        expect(getByText("Submit")).toBeEnabled();
    });

    it("should complete all answers with keyboard", async () => {
        const { getByText, getByTestId } = render(<ValidationForm {...defaultProps} />);
        await waitFor(() => getByTestId(validationFormId));

        for(let i = 0; i < mockChecks.length; i++) {
            userEvent.keyboard("{ArrowDown}");
            userEvent.keyboard("Digit$1");
        }

        expect(getByText("Submit")).toBeEnabled();
    });
});
